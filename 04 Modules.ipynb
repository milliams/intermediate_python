{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Modules\n",
    "\n",
    "In the last chapter we saw how we can take sections of code and make them reusable in different contexts without having to copy and paste the code. By using functions we give the code block a name and define what it needs in order to do its job.\n",
    "\n",
    "Functions are a step in the right direction but they still have the problem that to use functions from one script in another you'll have to copy and paste them over. The answer to this is to move the functions to a common location which both scripts can access. The Python solution for this is *modules*. Just like we used modules from the Python standard library earlier, we can create our own modules too.\n",
    "\n",
    "Continuing with the example of the `ounces_to_grams` function from the last chapter, let's see how we can use the function in other code, outside of that one script.\n",
    "\n",
    "To demonstrate this, let's look at the code in our `convert.py` file and think about the different role that parts of it are playing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": [
     "remove_cell"
    ]
   },
   "outputs": [],
   "source": [
    "%load_ext interactive_system_magic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "raw_mimetype": "text/x-python",
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "\n",
    "weight_in_grams = ounces_to_grams(12)\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are two main categories of code in this file:\n",
    "\n",
    "1. the function, `ounces_to_grams` is the sort of code which is useful in many different situations. It would be strange if it were only ever used to convert $12$ ounces into grams, but rather it could be used in a calculator, a recipe website, an engineering tool etc.\n",
    "2. the code below which *uses* the function. This is specific code which has been written to solve *today's* problem of converting this specific value. This is just one context in which you might use the `ounces_to_grams` function.\n",
    "\n",
    "Let's continue this though and make it explicit by moving the code for today's problem into its own file, `recipe.py` and delete that same code from `convert.py`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting recipe.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile recipe.py\n",
    "\n",
    "weight_in_grams = ounces_to_grams(12)\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we run this new script now, you'll get an error that it doesn't know what `ounces_to_grams` is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "tags": [
     "raises-exception",
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Traceback (most recent call last):\n",
       "  File \"/home/matt/courses/intermediate_python/recipe.py\", line 2, in <module>\n",
       "    weight_in_grams = ounces_to_grams(12)\n",
       "NameError: name 'ounces_to_grams' is not defined"
      ]
     },
     "execution_count": 5,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/courses/intermediate_python/venv/bin/python3 recipe.py",
       "returncode": 1
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script recipe.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is because that function is indeed not defined in this script. We need to tell it to load the file which contains the function we want to use, using the `import` statement:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting recipe.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile recipe.py\n",
    "\n",
    "import convert\n",
    "\n",
    "weight_in_grams = convert.ounces_to_grams(12)\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "340.19399999999996 g"
      ]
     },
     "execution_count": 7,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/courses/intermediate_python/venv/bin/python3 recipe.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script recipe.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have done a few things. First, on the first line we have *imported* our module. This is Python's way of getting access to code which is inside other modules. You write the keyword `import` followed by a space and then the name of the module you want to import. You'll notice that the way we do this is identical to when we were importing `math` etc. earlier.\n",
    "\n",
    "The name of a module is the same as the name of the file but without the `.py` extension. So, since we saved our *file* above as `convert.py`, the name of the *module* is `convert`.\n",
    "\n",
    "A module in Python is just a text file with Python code in it. All scripts *can* be imported as modules but it makes sense to logically separate in your mind between \"library\" modules and \"script\" modules. Library modules are those which you import to get access to the code inside them. Script modules are those which you run at the terminal using `python script.py`.\n",
    "\n",
    "Secondly, when calling the fucntion, we need to explicily state that we're using the `ounces_to_grams` function from the `convert` module using `convert.ounces_to_grams`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "- Make a module, `morse` (so the file name must be `morse.py`), and move the `encode` function from `encode.py` from the previous chapter  into it. Delete it from the original file.\n",
    "- Also move the definition of the dictionary `letter_to_morse` into `morse.py`.\n",
    "- Edit `encode.py` so that it imports the new module and calls the function from the new module.\n",
    "- [<small>answer</small>](answer_morse_module_encode.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Add into `morse.py` a function to convert Morse code to English. The code to do so is written below:\n",
    "\n",
    "```python\n",
    "# We need to invert the dictionary. This will create a dictionary\n",
    "# that can go from the morse back to the letter\n",
    "morse_to_letter = {}\n",
    "for letter in letter_to_morse:\n",
    "    morse = letter_to_morse[letter]\n",
    "    morse_to_letter[morse] = letter\n",
    "\n",
    "\n",
    "def decode(message):\n",
    "    english = []\n",
    "\n",
    "    # Now we cannot read by letter. We know that morse letters are\n",
    "    # separated by a space, so we split the morse string by spaces\n",
    "    morse_letters = message.split(\" \")\n",
    "\n",
    "    for letter in morse_letters:\n",
    "        english_letter = morse_to_letter[letter]\n",
    "        english.append(english_letter)\n",
    "\n",
    "    # Rejoin, but now we don't need to add any spaces\n",
    "    english_message = \"\".join(english)\n",
    "    \n",
    "    return english_message\n",
    "```\n",
    "\n",
    "Then write a new script `decode.py` which imports `morse`, calls the `decode` function and decodes the message `\"... . -.-. .-. . - / -- . ... ... .- --. .\"`.\n",
    "\n",
    "[<small>answer</small>](answer_morse_module_decode.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing\n",
    "\n",
    "So far we've been writing code and running it, but have you actually checked that the code is doing the right thing? Testing code is a very important part of the software development process because if you want other people to trust your work, you need to show that you've checked that your code does what you claim.\n",
    "\n",
    "There are more formal methods for software testing but we'll start with a common technique used with encoders/decoders and that is the *round-trip*. If we take a message, convert it to morse code and then convert it back, we should end up with the message we started with.\n",
    "\n",
    "Make a new file called `test_morse.py`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting test_morse.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile test_morse.py\n",
    "\n",
    "import morse\n",
    "\n",
    "message = \"sos we have hit an iceberg\"\n",
    "\n",
    "code = morse.encode(message)\n",
    "decode = morse.decode(code)\n",
    "\n",
    "print(message == decode)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you run it, you should see `True` printed to the console terminal tells us that the round-trip was successful."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n"
     ]
    }
   ],
   "source": [
    "%run test_morse.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll cover move on how to write tests for your Python modules in the [Best Practices in Sofware Engineering](https://milliams.com/courses/software_engineering_best_practices/) course but for now, this round-trip will suffice."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
