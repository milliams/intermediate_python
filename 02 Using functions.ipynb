{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1743e5ef-1a95-494e-ab3a-b8b2e9365582",
   "metadata": {},
   "source": [
    "# Using functions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17bea7d4-c37c-48ad-9c3d-a1d5b4eca73f",
   "metadata": {},
   "source": [
    "You're likely already comfortable with calling functions in Python. One of the first things you were probably taught was how to `print()` something. As you've continued your learning, you've probably come across a larger number of other ways of calling functions which are similar, but work slightly differently.\n",
    "\n",
    "In this section we're going to explore how Python works in these situations and how you can understand what's going on."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ad6a5ab-81db-48e0-9dec-5914c9443259",
   "metadata": {},
   "source": [
    "In terms of *calling* functions, you have seen:\n",
    "\n",
    "- free functions like `print()` and `range()`\n",
    "- functions on objects (also called \"methods\") like `my_list.append()` and `my_str.split()`\n",
    "\n",
    "but you will also have written things like the following in your code:\n",
    "\n",
    "- data *literals* like `10`, `\"hello\"` and `[1, 2]`\n",
    "- *syntax* like `for house in town:` and `if height > 10:`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77eb6afd-6bf4-4d56-bbe5-525938c87a05",
   "metadata": {},
   "source": [
    "So far you have probably learned these topics by following tutorials or attending workshops like this one. However, there is a central, official place where all the details of how these things work is written down so let's have an explore.\n",
    "\n",
    "Go to https://docs.python.org\n",
    "\n",
    "There are a lot of different sections linked from that page, but the two most important links for us are:\n",
    "1. The \"Language Reference\" (https://docs.python.org/3/reference/) which details all the in-built features of the language. It's quite technical and not the place you'd normally go to learn as a beginner. This covers our \"literals\" and \"syntax\" from above.\n",
    "2. The \"Library Reference\" (https://docs.python.org/3/library/) which covers all of the extra functions, classes and modules that Python provides. This is where we find all the cool stuff."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc088d37-2243-4e3e-b55f-5881c576678d",
   "metadata": {},
   "source": [
    "## Built-in functions\n",
    "\n",
    "Python comes with a bunch of functionality that you can use without having to explicitly enable anything. This includes things like integers, strings, lists, `print()`ing, file `open()`ing etc.\n",
    "\n",
    "First, lets have a peek at the \"built-in functions\". There is a page describing these under the \"Library Reference\" in a page called [Built-in Functions](https://docs.python.org/3/library/functions.html).\n",
    "\n",
    "Here we find the documentation for many of the functions we've already been using, for example [`print()`](https://docs.python.org/3/library/functions.html#print) and [`max()`](https://docs.python.org/3/library/functions.html#max).\n",
    "\n",
    "It is worth, over time, becoming familiar with the various functions that are available here. Some are quite useful for everyday work and some are more niche."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba1c6afe-4c18-4ae4-b08f-cc209a1b9a7f",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise 1\n",
    "\n",
    "Copy the following code into a new file called `hello.py`:\n",
    "\n",
    "```python\n",
    "my_name = ...\n",
    "\n",
    "name_length = ...\n",
    "\n",
    "print(f\"Hello {my_name}! Your name is {name_length} characters long\")\n",
    "```\n",
    "\n",
    "Replace the first `...` with a call to a built-in function which will read input from the person running the script.\n",
    "Replace the second `...` with a call to a function which will give the length of the string returned by the first.\n",
    "Search through the [built-in functions page](https://docs.python.org/3/library/functions.html) to find the appropriate functions.\n",
    "\n",
    "The script should, when run with `python hello.py`, then print out:\n",
    "\n",
    "```\n",
    "Please tell me your name: \n",
    "```\n",
    "\n",
    "and wait for you to type your name like:\n",
    "\n",
    "```\n",
    "Please tell me your name: Matt\n",
    "```\n",
    "\n",
    "After pressing enter, it should then print out:\n",
    "\n",
    "```\n",
    "Please tell me your name: Matt\n",
    "Hello Matt! Your name is 4 characters long\n",
    "```\n",
    "\n",
    "[<small>answer</small>](answer_hello_name.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a9852df-f15b-4a9b-9326-58cac653c2ab",
   "metadata": {},
   "source": [
    "## Built-in types\n",
    "\n",
    "As well as the free-standing built-in functions, Python has a bunch of built-in *data types*. This includes the data types you've already been using like integers, strings and lists. The details of them all are under the \"Library Reference\" on a page called [Built-in Types](https://docs.python.org/3/library/stdtypes.html).\n",
    "\n",
    "These types being built-in means that you don't need to explicitly enable their use and most have core-language syntax to create them.\n",
    "\n",
    "For example, the code\n",
    "\n",
    "```python\n",
    "animal = \"horse\"\n",
    "```\n",
    "\n",
    "creates a variable called `animal` from the string literal `\"horse\"` which is of the type `str`. This data type is built into the language and so the functionality that it has is documented on the [built-in types page under `str`](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str).\n",
    "\n",
    "This means that when we do:\n",
    "\n",
    "```python\n",
    "animal.capitalize()\n",
    "```\n",
    "\n",
    "it is looking at the data type of the variable `animal`, seeing that it is a `str` and then using the `capitalize` function that's available for that type to do the work."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01e9ee5e-d79c-4367-9225-2c8ab297b1e6",
   "metadata": {
    "tags": []
   },
   "source": [
    "The documentation for all the built-in types is all on that page so it's the place to go to check what you can do with a `str`, an `int`, a `list` or a `dict`. There's also a few other built-in types you might want to look into in the future such as `complex`, `set` and `tuple`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a71ce010-2263-4542-a45c-5a1b61a980a2",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise 2\n",
    "\n",
    "Have a look at the documentation page for the [functions that you can call on strings](https://docs.python.org/3/library/stdtypes.html#string-methods).\n",
    "\n",
    "Experiment with one or two and see if you can understand the documentation. Start by trying to answer the following:\n",
    "\n",
    "1. Given a string like `s = \"what is your name\"`, find a function which can split `s` into a list like `[\"what\", \"is\", \"your\", \"name\"]`\n",
    "2. Given a list like `[\"a\", \"b\", \"c\"]`, find a function which can join it together into a string like `\"a-b-c\"`\n",
    "\n",
    "[<small>answer</small>](answer_string_methods.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "52ff70db-1a02-4023-907b-5354c2cbbcf5",
   "metadata": {},
   "source": [
    "## Importing functions from modules\n",
    "\n",
    "While there are a good number of built-in functions and types, and you can go a long way without needing anything more, they are ultimately limited. Luckily, Python has a \"batteries included\" philosophy and provides a lot of additional functionality in its \"standard library\".\n",
    "\n",
    "The functionality provided by the standard library is provided in a series of *modules*, each of whch serves a particular purpose. The things in the standard library are always installed in any version of Python you have and you can rely on them being there.\n",
    "\n",
    "Note that even though they are always accessible, they do not count as \"built-in\" as in Python terms, that means something which you can use without having to access any extra modules."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef9c4894-7ab6-4fbe-9804-6a28c4c59e5b",
   "metadata": {},
   "source": [
    "### The `math` module\n",
    "\n",
    "Let's start by looking at one of the modules. On the [Standard Library](https://docs.python.org/3/library/) page, if you scroll past the \"built-in\" sections you'll find a set of groups of modules. There are about 200 modules in total, some very useful and some very niche.\n",
    "\n",
    "If you carry on down the page you'll see, \"[`math` — Mathematical functions](https://docs.python.org/3/library/math.html)\" which is where we are going to look first.\n",
    "\n",
    "This module provides a bunch of mathematical tools such as averages, trigonometry etc.\n",
    "\n",
    "You can get access to the module by *importing* it by name:\n",
    "\n",
    "```python\n",
    "import math\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13e41440-cee3-4bd8-9a3c-d889c212adf7",
   "metadata": {},
   "source": [
    "Once it is imported, you can use any of the functions inside it by typing the name of the module, followed by a dot, followed by the name of the function. So to call the [square root function](https://docs.python.org/3/library/math.html#math.sqrt) you would do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "511aae23-f73c-4adb-9869-9cf5f8ccb97b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting imports.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile imports.py\n",
    "\n",
    "import math\n",
    "\n",
    "print(math.sqrt(25))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "768d24ca-2e3e-48b3-aefb-af9e51b096bd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5.0\n"
     ]
    }
   ],
   "source": [
    "%run imports.py"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da342d0f-3ebd-4041-a942-1cba17fbc1b1",
   "metadata": {},
   "source": [
    "You can think of this as saying \"from the `math` module that I've just imported, get the `sqrt` function inside it and call it\"."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "51d28704-7e19-40b6-862e-fea04fb1664f",
   "metadata": {},
   "source": [
    "If you want to grab a specific function out of a module so that you can call it without specifying the module name, you can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "193520c8-e25f-4f73-bd17-d04c10c8cb0c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting imports.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile imports.py\n",
    "\n",
    "from math import sqrt\n",
    "\n",
    "print(sqrt(25))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "37718595-717c-425e-85c1-14ead608c25e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5.0\n"
     ]
    }
   ],
   "source": [
    "%run imports.py"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7bad7ee4-e569-4140-94d2-1f19c262f97b",
   "metadata": {},
   "source": [
    "We've seen two examples of places where dot `.` is used when calling functions in Python:\n",
    "\n",
    "1. calling a method on a variable like with `my_list.append()` or `my_string.split()`,\n",
    "2. calling a function from an imported module like `math.sqrt()`.\n",
    "\n",
    "In both these cases the dot is doing a very similar job. It's saying \"look inside the thing on the left of the dot for a thing called ...\". In some cases, it's looking inside a data type, and in other it's looking inside a module."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3908097-71a5-4ca8-bf95-aa08b4acaca9",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise 3\n",
    "\n",
    "Change the message in `encode.py` to use both upper and lower case letters:\n",
    "\n",
    "```python\n",
    "message = \"SOS We have hit an iceberg and need help quickly\"\n",
    "```\n",
    "\n",
    "When you now run the script with `python encode.py` you will find that it gives you a `KeyError`. This is because it is looking for an upper case \"S\" in the dictionary `letter_to_morse` and not finding one (dictionary keys are case-sensitive).\n",
    "\n",
    "Read through the documentation for the [string methods](https://docs.python.org/3/library/stdtypes.html#string-methods) to find one that might help convert the letter you have into one that matches the keys in the dictionary. You should be able to add a single line of code in the loop straight after `for letter in message:`.\n",
    "\n",
    "[<small>answer</small>](answer_morse_case.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
