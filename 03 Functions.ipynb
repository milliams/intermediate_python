{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Writing functions\n",
    "\n",
    "In the exercise at the end of the last chapter, you edited a script that could encode messages to Morse code. The script is good, but is not very easy to use or reusable. For someone to make use of the script, they will have to edit it and copy and paste your code every time they want to encode a different message.\n",
    "\n",
    "Functions provide a way of packaging code into reusable and easy-to-use components. We saw plenty of examples of functions in the last chapter, e.g. `print()` wraps up all the logic about exactly how to print things, all you need to do is pass in some arguments and it handles the rest. Likewise with `math.sqrt()`, you don't need to understand the algorithm it uses, simply what it needs you to pass it, and what it returns back to you.\n",
    "\n",
    "You can also bundle up your own logic into functions, allowing you to avoid repeating yourself and make your code easier to read. To explain how they work, lets imagine we are writing some code to help us with baking recipes. Often you will need to convert between different units, for example from ounces to grams. Open a new text file, name it `convert.py` and type the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "weight_in_ounces = 6\n",
    "\n",
    "weight_in_grams = weight_in_ounces * 28.3495\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "170.09699999999998 g\n"
     ]
    }
   ],
   "source": [
    "%run convert.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see this script as having three main parts to it:\n",
    "- The **set-up** where we define `weight_in_ounces`\n",
    "- The **data-processing** section where we read our inputs and create an output\n",
    "- The **output** section where we print our result to the screen\n",
    "\n",
    "The data processing section will work regardless of what data is inside the variable `weight_in_ounces` and so we can grab that bit of code and make it usable in other contexts quite easily, using functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining functions\n",
    "\n",
    "We can turn this into a function that can add any two arrays together by using `def`. To do this, type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This has created a new function called `ounces_to_grams` which we can now call. In a similar fashion to other constructs in Python (like `for` loops and `if` statements) it has a rigid structure.\n",
    "\n",
    "First we must use the `def` keyword to start a function definition:\n",
    "\n",
    "<pre>\n",
    " ↓\n",
    "<b style=\"color:darkred\">def</b> ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "</pre>\n",
    "\n",
    "Then we specify the name that we want to give the function. Like anything in Python, choose a descriptive name that describes what it does. This is the name which we will use when *calling* the function:\n",
    "\n",
    "<pre>\n",
    "           ↓\n",
    "def <b style=\"color:darkred\">ounces_to_grams</b>(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "</pre>\n",
    "\n",
    "Function definitions must then be followed by a pair of round brackets. This is a similar syntax to that used when *calling* a function and giving it arguments but here we're just defining it:\n",
    "\n",
    "<pre>\n",
    "                   ↓      ↓\n",
    "def ounces_to_grams<b style=\"color:darkred\">(</b>weight<b style=\"color:darkred\">)</b>:\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "</pre>\n",
    "\n",
    "Between those brackets go the names of the parameters we want the function to accept. We can define zero or more parameters. Here we are defining one:\n",
    "\n",
    "<pre>\n",
    "                      ↓\n",
    "def ounces_to_grams(<b style=\"color:darkred\">weight</b>):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "</pre>\n",
    "\n",
    "Finally, the line is completed with a colon:\n",
    "\n",
    "<pre>\n",
    "                           ↓\n",
    "def ounces_to_grams(weight)<b style=\"color:darkred\">:</b>\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "</pre>\n",
    "\n",
    "Since we've used a colon, we must indent the body of the function as we did with loops and conditional statements:\n",
    "\n",
    "<pre>\n",
    "def ounces_to_grams(weight):\n",
    "    <b style=\"color:darkred\">new_weight = weight * 28.3495</b>  ← body of<b style=\"color:darkred\">\n",
    "    return new_weight</b>              ← function\n",
    "</pre>\n",
    "\n",
    "Most functions will also want to return data back to the code that called it. You can choose what data is returned using the `return` keyword followed by the data you want to return:\n",
    "\n",
    "<pre>\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    <b style=\"color:darkred\">return</b> new_weight\n",
    "      ↑\n",
    "</pre>\n",
    "\n",
    "The body of the function has been copied from our script above with the only change being that the variables have different names.\n",
    "\n",
    "## Calling functions\n",
    "\n",
    "You can now call the function using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "\n",
    "weight_in_ounces = 6\n",
    "\n",
    "weight_in_grams = ounces_to_grams(weight_in_ounces)\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "170.09699999999998 g\n"
     ]
    }
   ],
   "source": [
    "%run convert.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case you have called the function `ounces_to_grams` and passed in the argument `weight_in_ounces`.\n",
    "\n",
    "In the fuction, `weight_in_ounces` is copied to its internal variable, `weight`. The function `ounces_to_grams` then acts on `weight`, creating the new varaible `new_weight`.\n",
    "\n",
    "It then returns `new_weight`, which is assigned to `weight_in_grams`.\n",
    "\n",
    "You can use your new `ounces_to_grams` function to convert any numbers. Try typing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "\n",
    "weight_in_ounces = 999\n",
    "\n",
    "weight_in_grams = ounces_to_grams(weight_in_ounces)\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "28321.1505 g\n"
     ]
    }
   ],
   "source": [
    "%run convert.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we can pass the values to the function directly, e.g. type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "\n",
    "weight_in_grams = ounces_to_grams(12)\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "340.19399999999996 g\n"
     ]
    }
   ],
   "source": [
    "%run convert.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Take the following code:\n",
    "\n",
    "```python\n",
    "my_num = 10\n",
    "\n",
    "doubled = my_num * 2\n",
    "\n",
    "print(doubled)\n",
    "```\n",
    "\n",
    "and convert the multiplication part to a function called `double` which can be called like:\n",
    "\n",
    "```python\n",
    "doubled = double(my_num)\n",
    "```\n",
    "\n",
    "such that when run, the code prints:\n",
    "\n",
    "```\n",
    "20\n",
    "```\n",
    "\n",
    "[<small>answer</small>](answer_function_double.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise # (bonus)\n",
    "\n",
    "Take the following code:\n",
    "\n",
    "```python\n",
    "my_list = [5, 7, 34, 5, 3, 545]\n",
    "\n",
    "big_numbers = []\n",
    "for num in my_list:\n",
    "    if num > 10:\n",
    "        big_numbers.append(num)\n",
    "\n",
    "print(big_numbers)\n",
    "```\n",
    "\n",
    "and convert the data-processing parts to a function called `big` which can be called like:\n",
    "\n",
    "```python\n",
    "my_list = [5, 7, 34, 5, 3, 545]\n",
    "\n",
    "large_numbers = big(my_list)\n",
    "\n",
    "print(large_numbers)\n",
    "```\n",
    "\n",
    "giving\n",
    "\n",
    "```\n",
    "[34, 545]\n",
    "```\n",
    "\n",
    "Be careful to pay attention to the indentation, ensuring that it is consistent with the original code. Particularly, note that the `return` statement will cause the function to exit, so make sure that it doesn't run until after the loop has finished.\n",
    "\n",
    "[<small>answer</small>](answer_function_big_numbers.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How many arguments?\n",
    "\n",
    "Note that you must pass in the right number of arguments to a function. `ounces_to_grams` expects one arguments, so if you pass more or less, then that is an error. Try this now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "\n",
    "weight_in_grams = ounces_to_grams()  # We've removed the arguments to this function\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "tags": [
     "remove_cell"
    ]
   },
   "outputs": [],
   "source": [
    "%load_ext interactive_system_magic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "tags": [
     "raises-exception",
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Traceback (most recent call last):\n",
       "  File \"/home/matt/projects/courses/intermediate_python/convert.py\", line 6, in <module>\n",
       "    weight_in_grams = ounces_to_grams()  # We've removed the arguments to this function\n",
       "                      ^^^^^^^^^^^^^^^^^\n",
       "TypeError: ounces_to_grams() missing 1 required positional argument: 'weight'"
      ]
     },
     "execution_count": 12,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/intermediate_python/venv/bin/python3 convert.py",
       "returncode": 1
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script convert.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "so as you can see, it tells you that you've given it the wrong number of arguments. It expects 1 (`weight`). Likewise, if you give too many arguments you get a similar error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "\n",
    "weight_in_grams = ounces_to_grams(12, 10)  # We've passed too many arguments now\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "tags": [
     "raises-exception",
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Traceback (most recent call last):\n",
       "  File \"/home/matt/projects/courses/intermediate_python/convert.py\", line 6, in <module>\n",
       "    weight_in_grams = ounces_to_grams(12, 10)  # We've passed too many arguments now\n",
       "                      ^^^^^^^^^^^^^^^^^^^^^^^\n",
       "TypeError: ounces_to_grams() takes 1 positional argument but 2 were given"
      ]
     },
     "execution_count": 14,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/intermediate_python/venv/bin/python3 convert.py",
       "returncode": 1
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script convert.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to define functions that take no arguments:\n",
    "\n",
    "```python\n",
    "def pi():\n",
    "    return 3.14159\n",
    "\n",
    "answer = pi()\n",
    "```\n",
    "\n",
    "single arguments:\n",
    "\n",
    "```python\n",
    "def double(x):\n",
    "    return x * 2\n",
    "\n",
    "answer = double(4)\n",
    "```\n",
    "\n",
    "or lots of arguments:\n",
    "\n",
    "```python\n",
    "def lots_of_args(a, b, c, d, e):\n",
    "    return {\"a\": a, \"b\": b, \"c\": c, \"d\": d, \"e\": e}\n",
    "\n",
    "answer = lots_of_args(1, 2, 3, 4, 5)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Take `encode.py` from the previous chapter and edit it so that the part that does the conversion (everything from `morse = []` to `\" \".join(morse)`) is moved into a function called `encode`. The function should take one argument and return the encoded morse string.\n",
    "\n",
    "To be more explicit, replace the following lines of code:\n",
    "\n",
    "```python\n",
    "morse = []\n",
    "\n",
    "for letter in message:\n",
    "    letter = letter.lower()\n",
    "    morse_letter = letter_to_morse[letter]\n",
    "    morse.append(morse_letter)\n",
    "\n",
    "morse_message = \" \".join(morse)\n",
    "```\n",
    "\n",
    "and replace them with:\n",
    "\n",
    "```python\n",
    "def encode(message):\n",
    "    ...\n",
    "    \n",
    "    return ...\n",
    "\n",
    "morse_message = encode(message)\n",
    "```\n",
    "\n",
    "where the `...` should be replaced with the code to do the conversion and the variable to be returned.\n",
    "\n",
    "[<small>answer</small>](answer_morse_encode_function.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise",
     "remove_cell"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "- Make a second file called `decode.py` with the contents:\n",
    "  ```python\n",
    "  letter_to_morse = {\n",
    "      'a':'.-', 'b':'-...', 'c':'-.-.', 'd':'-..', 'e':'.', 'f':'..-.', \n",
    "      'g':'--.', 'h':'....', 'i':'..', 'j':'.---', 'k':'-.-', 'l':'.-..', 'm':'--', \n",
    "      'n':'-.', 'o':'---', 'p':'.--.', 'q':'--.-', 'r':'.-.', 's':'...', 't':'-',\n",
    "      'u':'..-', 'v':'...-', 'w':'.--', 'x':'-..-', 'y':'-.--', 'z':'--..',\n",
    "      '0':'-----', '1':'.----', '2':'..---', '3':'...--', '4':'....-',\n",
    "      '5':'.....', '6':'-....', '7':'--...', '8':'---..', '9':'----.', ' ':'/'\n",
    "  }\n",
    "\n",
    "  # We need to invert the dictionary. This will create a dictionary\n",
    "  # that can go from the morse back to the letter\n",
    "  morse_to_letter = {}\n",
    "  for letter in letter_to_morse:\n",
    "      morse = letter_to_morse[letter]\n",
    "      morse_to_letter[morse] = letter\n",
    "\n",
    "  message = \"... --- ... / .-- . / .... .- ...- . / .... .. - / .- -. / .. -.-. . -... . .-. --. / .- -. -.. / -. . . -.. / .... . .-.. .--. / --.- ..- .. -.-. -.- .-.. -.--\"\n",
    "\n",
    "  english = []\n",
    "\n",
    "  # Now we cannot read by letter. We know that morse letters are\n",
    "  # separated by a space, so we split the morse string by spaces\n",
    "  morse_letters = message.split(\" \")\n",
    "\n",
    "  for letter in morse_letters:\n",
    "      english.append(morse_to_letter[letter])\n",
    "\n",
    "  # Rejoin, but now we don't need to add any spaces\n",
    "  english_message = \"\".join(english)\n",
    "  \n",
    "  print(english_message)\n",
    "  ```\n",
    "- Edit `decode.py` so that the part that does the conversion (everything from `english = []` to `\"\".join(english)`) is moved into a function. The function should take one argument, `message` and return the decoded english message. Choose a sensible, one word name for the function\n",
    "\n",
    "[<small>answer</small>](answer_morse_decode_function.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
