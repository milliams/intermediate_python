{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Errors and Exceptions\n",
    "\n",
    "*Exceptions* are Python's error-handling system. You've probably come across exceptions as that's how Python lets you know when you've done something wrong! They're not just there to tell you off, a good error message is to tell you that the computer has got into a situation where it doesn't know what to do and is asking you, the programmer, to help it by giving more information.\n",
    "\n",
    "Let's start by considering our function from before which converts data from ounces to grams:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": [
     "remove_cell"
    ]
   },
   "outputs": [],
   "source": [
    "%load_ext interactive_system_magic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting recipe.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile recipe.py\n",
    "\n",
    "import convert\n",
    "\n",
    "weight_in_grams = convert.ounces_to_grams(10)\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "283.495 g"
      ]
     },
     "execution_count": 4,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/courses/intermediate_python/venv/bin/python3 recipe.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script recipe.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Everything is looking good in this case, so let's have a look in some more detail."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Checking the value of data\n",
    "\n",
    "When you create a function you are creating a sort of promise. In this case, you're promising that if you call the function `ounces_to_grams` that it will \"take whatever value you passed as an argument, interpret it as a mass in grams and return the corresponding mass in ounces\". It might seem silly to write that out in full like that but it's a useful exercise to help make decisions about what the function should do in potentially unexpected situations.\n",
    "\n",
    "For example, what should happen if you passed the string `\"banana\"`? How exactly should the function \"interpret it as a mass in grams\"? You're writing the function so you get to decide the answer to that question. You could decide that it's reasonable to go and lookup the average mass of a banana in some database and return the value in ounces. It's your choice.\n",
    "\n",
    "I would suggest, however, that most users of your function would not expect that and it's more likely that it's a mistake that they've made. Good functions are easily explained and predictable and avoid guessing what the user meant. How though, can we let the person who called our function that there is a problem?\n",
    "\n",
    "Let's look at a simpler example to demonstrate how we can write some code in our funciton to help the person calling it understand if they make a mistake. Our function converts masses and since there's no such thing as a negative mass we need to decide what will happen if someone passes in a negative value. At the moment it will just return a negative mass:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting recipe.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile recipe.py\n",
    "\n",
    "import convert\n",
    "\n",
    "weight_in_grams = convert.ounces_to_grams(-30)\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-850.485 g"
      ]
     },
     "execution_count": 6,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/courses/intermediate_python/venv/bin/python3 recipe.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script recipe.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since we're the ones writing the function, we get to decide what happens. For the purpose of this section, we want a negative mass to be impossible so we need to add in some code to check for it.\n",
    "\n",
    "The easiest place to start is by adding an `if` statement and printing out an informative message:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    if weight < 0:\n",
    "        print(\"Cannot convert negative mass\")\n",
    "    \n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Cannot convert negative mass\n",
       "-850.485 g"
      ]
     },
     "execution_count": 8,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/courses/intermediate_python/venv/bin/python3 recipe.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script recipe.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see the message printed out, but the function has gone ahead and still returned the negative mass. What we want is a way to have the function display the message and stop running:\n",
    "\n",
    "```python\n",
    "def ounces_to_grams(weight):\n",
    "    if weight < 0:\n",
    "        print(\"Cannot convert negative mass\")\n",
    "        # ... at this point we want to stop running the function\n",
    "    \n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight\n",
    "```\n",
    "\n",
    "We could \"stop running the function\" at that point by `return`ing something, but _what_? If we return something like `0` or `-1` to designate the failure mode, then the person calling the function could quite easily carry on without noticing, assuming that it's a valid mass."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Raising exceptions\n",
    "\n",
    "Python solves this by allowing us to raise *exceptions*. An exception is an error message which the person calling the code *cannot* ignore which is useful as it helps prevent them write incorrect code.\n",
    "\n",
    "We can replace our `print` function call with the `raise` statement which we follow with the type of error we are notifying the user about.\n",
    "\n",
    "A list of all Python exception types is [here](http://docs.python.org/library/exceptions.html). It is important to choose the correct exception type for the error you're reporting. In our case, there is a problem with the value that the user is passing into our function, so I have chosen [`ValueError`](https://docs.python.org/3/library/exceptions.html#ValueError):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting convert.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile convert.py\n",
    "\n",
    "def ounces_to_grams(weight):\n",
    "    if weight < 0:\n",
    "        raise ValueError(\"Cannot convert negative mass\")\n",
    "    \n",
    "    new_weight = weight * 28.3495\n",
    "    return new_weight"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now when we run our code, it will display the error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "tags": [
     "raises-exception",
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Traceback (most recent call last):\n",
       "  File \"/home/matt/courses/intermediate_python/recipe.py\", line 4, in <module>\n",
       "    weight_in_grams = convert.ounces_to_grams(-30)\n",
       "  File \"/home/matt/courses/intermediate_python/convert.py\", line 4, in ounces_to_grams\n",
       "    raise ValueError(\"Cannot convert negative mass\")\n",
       "ValueError: Cannot convert negative mass"
      ]
     },
     "execution_count": 10,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/courses/intermediate_python/venv/bin/python3 recipe.py",
       "returncode": 1
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script recipe.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the result of the function has not been displayed. This is because Python will stop running the code in the cell or script once an exception is raised. To prove this, see what happens if you print something after the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting recipe.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile recipe.py\n",
    "\n",
    "import convert\n",
    "\n",
    "weight_in_grams = convert.ounces_to_grams(-30)\n",
    "\n",
    "print(f\"this will never be printed\")\n",
    "\n",
    "print(f\"{weight_in_grams} g\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "tags": [
     "raises-exception",
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Traceback (most recent call last):\n",
       "  File \"/home/matt/courses/intermediate_python/recipe.py\", line 4, in <module>\n",
       "    weight_in_grams = convert.ounces_to_grams(-30)\n",
       "  File \"/home/matt/courses/intermediate_python/convert.py\", line 4, in ounces_to_grams\n",
       "    raise ValueError(\"Cannot convert negative mass\")\n",
       "ValueError: Cannot convert negative mass"
      ]
     },
     "execution_count": 12,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/courses/intermediate_python/venv/bin/python3 recipe.py",
       "returncode": 1
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%run_python_script recipe.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This means that we cannot accidentally ignore the error and use the erroneous value.\n",
    "\n",
    "You've almost certainly seen error message like this crop up when writing your own code. Hopefully, now that you know how to create them yourself, they've become a little less scary!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "If we give our Morse code functions a string which can't be converted, e.g. `\"hi!\"`, `\"m'aidez\"` or `\"😀🐍\"` then we expect it to result in an error.\n",
    "\n",
    "Add a check to the start of the `encode` function to raise an exception if the passed `message` is not valid.\n",
    "\n",
    "At this point, don't worry about writing a check that's perfect, just think of *one thing* that you would check for and raise an exception if you see that. Start with some code that looks something like:\n",
    "\n",
    "```python\n",
    "if ... message ...:\n",
    "    raise ...\n",
    "```\n",
    "\n",
    "You'll also need to choose an appropriate exception type to raise. Take a look at the [documentation](https://docs.python.org/3/library/exceptions.html) and find the exception which makes sense if an incorrect value is passed in.\n",
    "\n",
    "[<small>answer</small>](answer_morse_raise.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
